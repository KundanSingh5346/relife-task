//
//  SignupVc.swift
//  RELife Task
//
//  Created by SMT on 02/03/21.
//  Copyright © 2021 MAC. All rights reserved.
//

import UIKit

class SignupVc: UIViewController {

    @IBOutlet weak var tfFirstName: UITextField!
    @IBOutlet weak var tfLastName: UITextField!
    @IBOutlet weak var tfNumber: UITextField!
    @IBOutlet weak var tfEmail: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func actionLogin(_ sender: UIButton) {
        _=self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionSignup(_ sender: UIButton) {
        if let strMsg = checkValidation() {
            Http.alert("", strMsg)
        } else {
            checkForEsixtUser()
        }
    }
    
}
