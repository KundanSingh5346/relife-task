//
//  SignupPresenter.swift
//  RELife Task
//
//  Created by SMT on 03/03/21.
//  Copyright © 2021 MAC. All rights reserved.
//

import UIKit

extension SignupVc: UITextFieldDelegate {
    
    func checkValidation() -> String? {
        if tfFirstName.text?.count == 0 {
            return AlertMsg.blankFirstName
        } else if tfLastName.text?.count == 0 {
            return AlertMsg.blankLastName
        } else if tfNumber.text?.count == 0 {
            return AlertMsg.blankNumber
        } else if tfEmail.text!.count > 0 && !tfEmail.text!.isEmail {
            return AlertMsg.invalidEmail
        }
        return nil
    }
    
    func checkForEsixtUser() {
        LocalDB().isExistUser(tfNumber.text!) { (success) in
            if success {
                Http.alert("", AlertMsg.alreadyRegistered)
            } else {
                self.jumpToVerification()
            }
        }
    }
    
    func jumpToVerification() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VerificationVc") as! VerificationVc
        vc.isLogin = true
        vc.mobileNumber = tfNumber.text!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: UITextField Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let length = textField.text!.count + string.count - range.length
        if textField == tfFirstName {
            return (length > 30) ? false : true
        } else if textField == tfLastName {
            return (length > 30) ? false : true
        } else if textField == tfEmail {
            return (length > 40) ? false : true
        } else if textField == tfNumber {
            return (length > 10) ? false : true
        }
        return true
    }
}
