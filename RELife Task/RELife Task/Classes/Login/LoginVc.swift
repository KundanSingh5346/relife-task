//
//  LoginVc.swift
//  RELife Task
//
//  Created by SMT on 02/03/21.
//  Copyright © 2021 MAC. All rights reserved.
//

import UIKit

class LoginVc: UIViewController {

    @IBOutlet weak var tfNumber: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func actionSignup(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignupVc") as! SignupVc
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionLogin(_ sender: UIButton) {
        createImagesFolder()
        if let strMsg = checkValidation() {
            Http.alert("", strMsg)
        } else {
            checkForEsixtUser()
        }
    }
    
    private func createImagesFolder() {
        
        
    }
}
