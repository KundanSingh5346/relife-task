//
//  LoginPresenter.swift
//  RELife Task
//
//  Created by SMT on 02/03/21.
//  Copyright © 2021 MAC. All rights reserved.
//

import UIKit

extension LoginVc: UITextFieldDelegate {
    
    func checkValidation() -> String? {
        if tfNumber.text?.count == 0 {
            return AlertMsg.blankNumber
        }
        return nil
    }
    
    func checkForEsixtUser() {
        LocalDB().isExistUser(tfNumber.text!) { (success) in
            if success {
                self.jumpToVerification()
            } else {
                Http.alert("", AlertMsg.notRegistered)
            }
        }
    }
    
    func jumpToVerification() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VerificationVc") as! VerificationVc
        vc.isLogin = true
        vc.mobileNumber = tfNumber.text!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: UITextField Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let length = textField.text!.count + string.count - range.length
        return (length > 10) ? false : true
    }
}

