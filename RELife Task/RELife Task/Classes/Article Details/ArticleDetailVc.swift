//
//  ArticleDetailVc.swift
//  RELife Task
//
//  Created by SMT on 04/03/21.
//  Copyright © 2021 MAC. All rights reserved.
//

import UIKit

class ArticleDetailVc: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblDetails: UILabel!
    
    var obArticle = ArticleModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = "Details"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.title = ""
    }
    
    @IBAction func actionDownload(_ sender: Any) {
        saveImage() 
    }
    
}
