//
//  ArticleDetailPresenter.swift
//  RELife Task
//
//  Created by SMT on 04/03/21.
//  Copyright © 2021 MAC. All rights reserved.
//

import UIKit
import SDWebImage

extension ArticleDetailVc {
    
    func configure() {
        lblTitle.text = obArticle.title
        imgView.sd_setImage(with: URL(string: obArticle.imageUrl), placeholderImage: UIImage(named: "no_image.jpg"))
        lblDetails.text = obArticle.summary
    }
    
    func saveImage() {
        
        SDWebImageManager.shared.loadImage(
            with: URL(string: obArticle.imageUrl),
          options: .highPriority,
          progress: nil) { (image, data, error, cacheType, isFinished, imageUrl) in
            if isFinished {
                if let image = image {
                    
                    let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                    let fileName = "image\(Int(Date().timeIntervalSince1970)).jpg"
                    let fileURL = documentsDirectory.appendingPathComponent(fileName)
                    if let data = image.jpegData(compressionQuality:  1.0),
                      !FileManager.default.fileExists(atPath: fileURL.path) {
                        LoadingView.shared.showOverlay(nil)
                        do {
                            try data.write(to: fileURL)
                            LoadingView.shared.hideOverlayView()
                            Http.alert("", "Image saved successfully.")
                        } catch {
                            LoadingView.shared.hideOverlayView()
                            Http.alert("", "Error saving file: \(error.localizedDescription)")
                        }
                    }
                    
                } else {
                    Http.alert("", error?.localizedDescription)
                }
            }
        }
        
        
    }
}
