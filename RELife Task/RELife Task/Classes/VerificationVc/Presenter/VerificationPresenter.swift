//
//  VerificationPresenter.swift
//  RELife Task
//
//  Created by SMT on 03/03/21.
//  Copyright © 2021 MAC. All rights reserved.
//

import UIKit
import OTPFieldView

extension VerificationVc: OTPFieldViewDelegate {
    
    
    
    func configure() {
        self.vwOtp.fieldsCount = 4
        self.vwOtp.fieldBorderWidth = 1.5
        self.vwOtp.defaultBorderColor = UIColor.black
        self.vwOtp.filledBorderColor = UIColor.green
        self.vwOtp.cursorColor = UIColor.red
        self.vwOtp.displayType = .roundedCorner
        self.vwOtp.fieldSize = 50
        self.vwOtp.separatorSpace = 8
        self.vwOtp.shouldAllowIntermediateEditing = false
        self.vwOtp.delegate = self
        self.vwOtp.initializeUI()
        self.vwOtp.filledBorderColor = .black
        self.vwOtp.defaultBorderColor = .black
        self.vwOtp.errorBorderColor = .black
    }
    
    func saveInDb() {
        
        if let arr = self.navigationController?.viewControllers {
            for vc in arr {
                if vc is SignupVc {
                    let vcc = vc as! SignupVc
                    LocalDB().saveUser(vcc.tfFirstName.text!, vcc.tfLastName.text!, vcc.tfNumber.text!, vcc.tfEmail.text!) { (success) in
                        if success {
                            Http.alert("", AlertMsg.registrationSuccess, [self, "Ok"])
                        } else {
                            Http.alert("", "Not registered.")
                        }
                    }
                }
            }
        }
    }
    
    override func alertZero() {
        self.moveToDashboard()
    }
    
    func moveToDashboard() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DashboardNav")
        SceneDelegate.shared?.window?.rootViewController = vc
    }
    
    //MARK: OTPFieldViewDelegate
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otp: String) {
        strOtp = otp
    }
    
    func hasEnteredAllOTP(hasEnteredAll: Bool) -> Bool {
        true
    }
}
