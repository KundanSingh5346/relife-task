//
//  VerificationVc.swift
//  RELife Task
//
//  Created by SMT on 03/03/21.
//  Copyright © 2021 MAC. All rights reserved.
//

import UIKit
import OTPFieldView

class VerificationVc: UIViewController {

    @IBOutlet weak var vwOtp: OTPFieldView!
    
    var isLogin = false
    var strOtp = ""
    var mobileNumber = ""
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
    
    @IBAction func actionBack(_ sender: Any) {
        _=self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionResend(_ sender: Any) {
    }
    
    @IBAction func actionSubmit(_ sender: Any) {
        if strOtp != "1234" {
            Http.alert("", AlertMsg.invalidOtp)
        } else if isLogin {
            Http.alert("", AlertMsg.loginSuccess, [self, "Ok"])
        } else {
            self.saveInDb()
        }
    }
}
