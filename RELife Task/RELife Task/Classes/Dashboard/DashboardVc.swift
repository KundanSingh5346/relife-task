//
//  DashboardVc.swift
//  RELife Task
//
//  Created by SMT on 04/03/21.
//  Copyright © 2021 MAC. All rights reserved.
//

import UIKit

class DashboardVc: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    
    var arrArticles = [ArticleModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = "Articles"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.title = ""
    }
    
}
