//
//  DashboardPresenter.swift
//  RELife Task
//
//  Created by SMT on 04/03/21.
//  Copyright © 2021 MAC. All rights reserved.
//

import UIKit

extension DashboardVc: UITableViewDelegate, UITableViewDataSource {
    
    func configure() {
        tblView.registerCell(nibName: "ArticleCell")
        tblView.tableFooterView = UIView()
        wsGetArticles()
    }
    
    //MARK: UITableViewDelegate & UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrArticles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleCell", for: indexPath) as! ArticleCell
        cell.configure(ob: arrArticles[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ArticleDetailVc") as! ArticleDetailVc
        vc.obArticle = arrArticles[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func wsGetArticles() {

        var request = URLRequest(url: URL(string: "https://test.spaceflightnewsapi.net/api/v2/articles")!)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        LoadingView.shared.showOverlay(nil)
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            print(response!)
            do {
                if let json = try JSONSerialization.jsonObject(with: data!) as? Array<Any> {
                    for i in 0..<json.count {
                        if let dict = json[i] as? [String:Any] {
                            self.arrArticles.append(ArticleModel(dict: dict))
                        }
                    }
                    DispatchQueue.main.async {
                        LoadingView.shared.hideOverlayView()
                        self.tblView.reloadData()
                    }
                }
            } catch {
                DispatchQueue.main.async {
                    LoadingView.shared.hideOverlayView()
                }
                print("error")
            }
        })

        task.resume()
    }
}
