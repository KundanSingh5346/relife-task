//
//  ArticleCell.swift
//  RELife Task
//
//  Created by SMT on 04/03/21.
//  Copyright © 2021 MAC. All rights reserved.
//

import UIKit
import SDWebImage

class ArticleCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    func configure(ob: ArticleModel) {
        lblTitle.text = ob.title
        imgView.sd_setImage(with: URL(string: ob.imageUrl), placeholderImage: UIImage(named: "no_image.jpg"))
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
