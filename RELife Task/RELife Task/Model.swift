//
//  Model.swift
//  RELife Task
//
//  Created by SMT on 02/03/21.
//  Copyright © 2021 MAC. All rights reserved.
//

import UIKit

class UserModel: NSObject {

    var firstName = ""
    var lastName = ""
    var number = ""
    var email = ""
}

class ArticleModel: NSObject {

    var featured = ""
    var id = ""
    var imageUrl = ""
    var newsSite = ""
    var publishedAt = ""
    var summary = ""
    var title = ""
    var updatedAt = ""
    var url = ""

    init(dict: [String:Any]) {
        self.featured = string(dict, "featured")
        self.id = string(dict, "id")
        self.imageUrl = string(dict, "imageUrl")
        self.newsSite = string(dict, "newsSite")
        self.publishedAt = string(dict, "publishedAt")
        self.summary = string(dict, "summary")
        self.title = string(dict, "title")
        self.updatedAt = string(dict, "updatedAt")
        self.url = string(dict, "url")
    }
    
    override init() {
    }
    
}
