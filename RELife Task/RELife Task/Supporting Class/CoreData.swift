//
//  CoreData.swift
//  Globussoft Task
//
//  Created by SMT on 19/02/21.
//  Copyright © 2021 MAC. All rights reserved.
//

import UIKit
import CoreData

class LocalDB: NSObject {

    let context = kAppDelegate.persistentContainer.viewContext
    
    func saveUser(_ firstName: String, _ lastName: String, _ number: String, _ email: String, completion: @escaping  (Bool) -> Swift.Void) {
        let entity = NSEntityDescription.entity(forEntityName: "Users", in: context)
        let newUser = NSManagedObject(entity: entity!, insertInto: context)
        
        newUser.setValue(firstName, forKey: "firstName")
        newUser.setValue(lastName, forKey: "lastName")
        newUser.setValue(number, forKey: "number")
        newUser.setValue(email, forKey: "email")

        print("Storing Data..")
        do {
            try context.save()
            completion(true)
        } catch {
            print("Storing data Failed")
        }
    }
     
    func isExistUser(_ number: String, completion: @escaping  (Bool) -> Swift.Void) {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        request.predicate = NSPredicate(format: "%K == %@", "number", number)

        let objects = try! context.fetch(request)
        if let objects = objects as? [NSManagedObject] {
            if objects.count > 0 {
                completion(true)
            } else {
                completion(false)
            }
        } else {
            completion(false)
        }
        
    }

}


