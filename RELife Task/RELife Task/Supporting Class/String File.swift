//
//  String File.swift
//  Globussoft Task
//
//  Created by SMT on 18/02/21.
//  Copyright © 2021 MAC. All rights reserved.
//

import UIKit

class AlertMsg: NSObject {
    static let blankFirstName = "Please enter first name."
    static let blankLastName = "Please enter last name."
    static let blankNumber = "Please enter phone number."
    static let invalidEmail = "Please enter valid email."
    static let blankPassword = "Please enter password."
    static let invalidOtp = "Please enter valid otp."
    static let notRegistered = "This mobile number is not registered. Please signup first. "
    static let alreadyRegistered = "This mobile number is already registered. Please login or use other number."
    static let registrationSuccess = "Registration successfully."
    static let loginSuccess = "Login successfully."

}
