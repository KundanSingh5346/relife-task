
import Foundation
import UIKit

open class LoadingView{
    var backGroundView : UIView!
    var overlayView : UIImageView!
    var activityIndicator : UIActivityIndicatorView!
    
    class var shared: LoadingView {
        struct Static {
            static let instance: LoadingView = LoadingView()
        }
        return Static.instance
    }
    
    init(){
        let window = SceneDelegate.shared!.window
        let frame = window!.rootViewController?.view.bounds
        self.backGroundView = UIView()
        self.backGroundView.frame = CGRect(x:0, y:0, width:(frame?.width)!, height:(frame?.height)!)
        self.backGroundView.backgroundColor = UIColor.clear
        self.overlayView = UIImageView()//UIView()
        self.activityIndicator = UIActivityIndicatorView()
    
        let imgView = UIImageView()
        imgView.frame = CGRect(x:0, y:0, width:(frame?.width)!, height:(frame?.height)!)
        imgView.alpha = 0.4
        imgView.backgroundColor = UIColor.white
        self.backGroundView.addSubview(imgView)
        
        overlayView.frame = CGRect(x:0, y:0, width:80, height:80)
        
        overlayView.backgroundColor = UIColor.lightGray
        overlayView.clipsToBounds = true
        overlayView.layer.cornerRadius = 10
        overlayView.layer.zPosition = 1
        
        self.backGroundView.addSubview(overlayView)
        activityIndicator.frame = CGRect(x:0, y:0, width:40, height:40)
        activityIndicator.center = CGPoint(x:overlayView.bounds.width / 2,y: overlayView.bounds.height / 2)
        activityIndicator.style = .whiteLarge
        overlayView.addSubview(activityIndicator)
    }
    
    open func showOverlay(_ view: UIView?) {
       DispatchQueue.main.async {
        let window = SceneDelegate.shared!.window!

        self.overlayView.center = (window.center)
        window.addSubview(self.backGroundView)
        window.rootViewController?.view.isUserInteractionEnabled = false
        self.activityIndicator.startAnimating()
        }
        
    }
    
    open func hideOverlayView() {
        DispatchQueue.main.async {
        self.activityIndicator.stopAnimating()
            let window = SceneDelegate.shared!.window
            window?.rootViewController?.view.isUserInteractionEnabled = true
        self.backGroundView.removeFromSuperview()
    }
    }
}
