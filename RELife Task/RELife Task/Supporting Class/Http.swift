

import UIKit

let kAppDelegate = UIApplication.shared.delegate as! AppDelegate

class Http: NSObject {

    class func alert (_ ttl:String?, _ msg:String?) {
        if (msg != nil) {
            if (msg?.count)! > 0 {
                DispatchQueue.main.async {
                    
                    let alertController:UIAlertController
                    
                    var ttl = ttl
                    
                    if (ttl == nil) {
                        ttl = ""
                    }
                    
                    alertController = UIAlertController(title: ttl, message: msg, preferredStyle: .alert)
                    
                    let defaultActionq = UIAlertAction(title: "Ok", style: .default, handler: { (_ action:UIAlertAction) in
                        
                    })
                    
                    alertController.addAction(defaultActionq)
                    
                    SceneDelegate.shared?.window?.rootViewController?.present(alertController, animated: true, completion: { })
                }
            }
        }
    }
    
    func alert (_ ttl:String?, _ msg:String?) {
        Http.alert(ttl, msg)
    }
    
    class func alert (_ ttl:String?, _ msg:String?, _ btns:[Any]) {
        if (msg != nil) {
            if (msg?.count)! > 0 {
                DispatchQueue.main.async {
                    
                    let alertController:UIAlertController
                    
                    var ttl = ttl
                    
                    if (ttl == nil) {
                        ttl = ""
                    }
                    
                    alertController = UIAlertController(title: ttl, message: msg, preferredStyle: .alert)
                    
                    if btns.count >= 2 {
                        alertController.addAction(self.alertAction(btns, 0))
                    }
                    
                    if btns.count >= 3 {
                        alertController.addAction(self.alertAction(btns, 1))
                    }
                   
                    SceneDelegate.shared?.window?.rootViewController?.present(alertController, animated: true, completion: { })
                }
            }
        }
    }
    
    class func alertAction (_ btns:[Any], _ index:Int) -> UIAlertAction {
        let action = UIAlertAction(title: (btns[index + 1] as? String)!, style: .default, handler: { (_ action:UIAlertAction) in
            
            let vc = btns[0] as? UIViewController
            
            if index == 0 {
                vc?.alertZero()
            } else if index == 1 {
                vc?.alertOne()
            }
        })
        
        return action
    }
    
    
    class func instance () -> Http {
        return Http()
    }
    
}

extension UIViewController {
    @objc func alertZero () {
        
    }
    
    @objc func alertOne () {
        
    }
}


func string (_ dict:[String:Any], _ key:String) -> String {
    if let title = dict[key] as? String {
        return "\(title)"
    } else if let title = dict[key] as? NSNumber {
        return "\(title)"
    } else {
        return ""
    }
}

func number (_ dict:[String:Any], _ key:String) -> NSNumber {
    if let title = dict[key] as? NSNumber {
        return title
    } else if let title0 = dict[key] as? String {
        let title = title0.trimmingCharacters(in: .whitespacesAndNewlines)
        if let title1 = Int(title) as Int? {
            return NSNumber(value: title1)
        } else if let title1 = Float(title) as Float? {
            return NSNumber(value: title1)
        } else if let title1 = Double(title) as Double? {
            return NSNumber(value: title1)
        } else if let title1 = Bool(title) as Bool? {
            return NSNumber(value: title1)
        }
        
        return 0
    } else {
        return 0
    }
}

func stringUrl (_ dict:[String:Any], _ key:String) -> String {
    var strUrl = ""
    
    if let title = dict[key] as? String {
        strUrl = title
    } else if let title = dict[key] as? NSNumber {
        strUrl = "\(title)"
    }
    return strUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
}

func bool (_ dict:[String:Any], _ key:String) -> Bool {
    if let title = dict[key] as? Bool {
        return title
    } else {
        return false
    }
}
